#!/bin/bash
set -o errexit
set -o nounset
set -o xtrace

echo 'First output, waiting for 3 seconds'
sleep 3s
echo 'Second output, waiting for 122 seconds'
sleep 122s
echo 'Third output, waiting for 122 seconds'
sleep 122s
echo 'Fourth output, waiting for 122 seconds'
sleep 122s
echo 'Fifth output, done'
