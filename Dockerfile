FROM bash

COPY entrypoint.sh /

CMD ["bash", "/entrypoint.sh"]
